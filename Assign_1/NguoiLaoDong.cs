﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign_1
{
    public class NguoiLaoDong
    {
        public string HoTen;
        public int NamSinh;
        public double LuongCoBan;

        public NguoiLaoDong(){

        }

        public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }

        public void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }

        public double TinhLuong()
        {
            return LuongCoBan;
        }

        public void XuatThongTin()
        {
            Console.WriteLine("Ho ten la: " + HoTen 
                + ", nam sinh: " + NamSinh 
                + ", luong co ban: " + LuongCoBan
                + ".");
        }
    }
}
