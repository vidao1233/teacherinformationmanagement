﻿using Assign_1;

public class Program
{
    public static void Main(string[] args)
    {
        Console.Write("Nhap so giao vien: ");
        int soLuong = Convert.ToInt32(Console.ReadLine());
        Console.Write("===============================================================================\n");

        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

        for (int i = 0; i < soLuong; i++)
        {
            Console.WriteLine("Nhap thong tin giao vien thu " + (i + 1));

            Console.Write("Nhap ho ten: ");
            string hoTen = Console.ReadLine();

            Console.Write("Nhap nam sinh: ");
            int namSinh = Convert.ToInt32(Console.ReadLine());

            Console.Write("Nhap luong co ban: ");
            double luongCoBan = Convert.ToDouble(Console.ReadLine());

            Console.Write("Nhap he so luong: ");
            double heSoLuong = Convert.ToDouble(Console.ReadLine());

            GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
            danhSachGiaoVien.Add(giaoVien);
            Console.Write("===============================================================================\n");
        }

        double luongThapNhat = double.MaxValue;
        GiaoVien giaoVienLuongThapNhat = null;

        foreach (GiaoVien giaoVien in danhSachGiaoVien)
        {
            if (giaoVien.TinhLuong() < luongThapNhat)
            {
                luongThapNhat = giaoVien.TinhLuong();
                giaoVienLuongThapNhat = giaoVien;
            }
        }
        Console.WriteLine("Thong tin giao vien co luong thap nhat:");
        if (giaoVienLuongThapNhat != null)
        {
            giaoVienLuongThapNhat.XuatThongTin();
        }

        Console.ReadKey();
    }
}